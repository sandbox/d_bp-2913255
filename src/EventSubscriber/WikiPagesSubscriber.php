<?php

namespace Drupal\wiki_pages\EventSubscriber;

use Drupal\node\Entity\Node;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Url;
use Drupal\wiki_pages\Helper\UserWikiPages;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Implementing EventSubscriberInterface.
 */
class WikiPagesSubscriber implements EventSubscriberInterface {

  /**
   * Getting SubscribedEvents.
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onResponse'];
    return $events;
  }

  /**
   * Checking onResponse if user is allowed to access the requested node.
   */
  public function onResponse(FilterResponseEvent $event) {
    $request = $event->getRequest();
    // Checking the route.
    if ($request->attributes->get('_route') == 'entity.node.canonical') {
      // Loading the requested node from request variable.
      $node = Node::load($request->get('node')->get('nid')->value);

      // Checking the content-type of node.
      if ($node->get('type')->target_id == 'wiki_page') {
        // If user is not allowed to access the node,
        // redirecting and setting the message.
        if (!UserWikiPages::isAllowed(UserWikiPages::findParent($node->id()))) {
          drupal_set_message('You are not authorised to access this page.', 'error');
          $url = Url::fromRoute('<front>', [], ['absolute' => TRUE])
            ->toString();
          $event->setResponse(new RedirectResponse($url));
        }
      }
    }
  }

}
