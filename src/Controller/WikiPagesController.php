<?php

namespace Drupal\wiki_pages\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Controller routines for page example routes.
 */
class WikiPagesController extends ControllerBase {

  /**
   * Query limit of wiki pages.
   */
  const QUERY_LIMIT = 20;

  /**
   * Characters limit when displaying description.
   */
  const TEXT_LIMIT = 400;

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'wiki_pages';
  }

  /**
   * Generating the wiki pages list.
   *
   * @return array
   *   Returning markup.
   */
  public function wikiPagesList() {
    // Getting the current user.
    $currentUser = User::load(\Drupal::currentUser()->id());

    // Building the query, and selecting the nodes from users_wikipages.
    $query = \Drupal::database()
      ->select('users_wikipages', 'uw')
      ->fields('uw')
      ->condition('uw.uid', $currentUser->id(), '=')
      ->fields('nfd', ['title']);
    // If user have no roles like administrator or wiki pages moderator,
    // Adding the condition, to check if user is allowed to access
    // to particular node.
    if (!in_array('administrator', $currentUser->getRoles()) && !in_array('wiki_pages_moderator', $currentUser->getRoles())) {
      $query->condition('allowed', 1, '=');
    }
    $query->orderBy('title', 'ASC');
    $query->join('node_field_data', 'nfd', 'nfd.nid = uw.id');

    // Creating the pager.
    $pager = $query
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(self::QUERY_LIMIT);

    $pageIds = $pager->execute()->fetchAll();
    // Loading each node in array $pages.
    foreach ($pageIds as $page) {
      $node = Node::load($page->id);
      $pages[$node->get('nid')->value] = $node;
    }

    // Generating output html markup.
    $output = '';
    // Adding the 'Add New page' button
    // If user have Wiki pages moderator or
    // Administrator role.
    if (in_array('wiki_pages_moderator', $currentUser->getRoles()) || in_array('administrator', $currentUser->getRoles())) {
      $output .= '<a href="/node/add/wiki_page" class="button">' . t('Add new page') . '</a>';
    }
    // Checking if there is any page (node)
    // Selected for the database, if not
    // Outputting the message.
    if (empty($pages)) {
      $output .= '<p><em>' . t('There are no pages yet.') . '</em></p>';
    }
    else {
      // Surrounding the pages with wiki-pages-list div.
      $output .= '<div class="wiki-pages-list">';
      // Generating the markup for each node.
      foreach ($pages as $page) {
        // Checking the description length
        // And adapting to TEXT_LIMIT length.
        $description = mb_strimwidth(strip_tags($page->get('field_wiki_page_description')->value), 0, self::TEXT_LIMIT, '...');

        $output .= '<div class="wiki-page">';
        // Outputting title.
        $output .= '<h2><a href="/node/' . $page->get('nid')->value . '">' . $page->getTitle() . '</a></h2>';
        // Outputting description.
        $output .= '<p>' . $description . '</p>';
        $output .= '<hr>';
        $output .= '</div>';
      }
      $output .= "</div>";
    }

    return [
    // Returning the generated markup.
      '#markup' => $output,
      'pager' => [
        '#type' => 'pager',
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Making sure that user have access to Grant Permission tab.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Passing the node as an argument.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   Returning an instance of AccessResult.
   */
  public function checkContentType(Node $node) {
    // Getting the current user.
    $currentUser = \Drupal::currentUser();
    // Checking the node content type.
    if ($node->get('type')->target_id == 'wiki_page') {
      // Checking if user is allowed to access.
      if ($node->get('field_wiki_page_parent')->target_id == NULL && (in_array('wiki_pages_moderator', $currentUser->getRoles()) || in_array('administrator', $currentUser->getRoles()))) {
        return AccessResult::allowed();
      }
      return AccessResult::forbidden();
    }
    return AccessResult::allowed();
  }

}
