<?php

namespace Drupal\wiki_pages\Helper;

use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Helper class for checking if user is allowed to acces the node.
 */
class UserWikiPages {

  /**
   * Checking if user is pre-selected.
   *
   * @param int $pageId
   *   Node ID.
   * @param int $uid
   *   User uid.
   *
   * @return int
   *   Returning 0/1 if user us pre-selected.
   */
  public static function isPreSelected($pageId, $uid) {
    // Building the query to check if user is 'selected' -> allowed.
    $exists_query = \Drupal::database()
      ->select('users_wikipages', 'uwp')
      ->fields('uwp', ['uid', 'id', 'allowed'])
      ->condition('uid', $uid, '=')
      ->condition('id', $pageId, '=')
      ->execute();
    $user = $exists_query->fetchAll();
    if ($user) {
      if ($user[0]->allowed) {
        return 1;
      }
      return 0;
    }
    return 0;
  }

  /**
   * Find parent scope.
   *
   * @param int $id
   *   ID of current node.
   *
   * @return int
   *   Returning an parent node id.
   */
  public static function findParent($id) {
    // Loading the node.
    $node = Node::load($id);
    // Defining parentId variable to store the id.
    static $parentId = NULL;
    if ($node == NULL) {
      return NULL;
    }
    // Checking if node have parent node.
    if ($node->get('field_wiki_page_parent')->target_id !== NULL) {
      self::findParent($node->get('field_wiki_page_parent')->target_id);
    }
    else {
      $parentId = $id;
    }
    return $parentId;
  }

  /**
   * Returns if user exists in users_wikipages.
   *
   * @param int $pageId
   *   Expects node id.
   * @param int $uid
   *   Expects user id.
   *
   * @return bool
   *   Returning true/false.
   */
  public static function exists($pageId, $uid) {
    // Building query to check if user is in db.
    $user = \Drupal::database()
      ->select('users_wikipages', 'uw')
      ->fields('uw', ['uid', 'id'])
      ->condition('uid', $uid, '=')
      ->condition('id', $pageId, '=')
      ->execute()
      ->fetchAll();
    if (!empty($user) || $user != NULL) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Checking if user is allowed to access.
   *
   * @param int $nodeId
   *   Node id.
   *
   * @return bool
   *   Returning true/false
   */
  public static function isAllowed($nodeId) {
    // Getting the current user.
    $currentUser = User::load(\Drupal::currentUser()->id());

    // Building the query to check if user is allowed to access.
    $query = \Drupal::database()
      ->select('users_wikipages', 'uw')
      ->fields('uw')
      ->condition('uid', $currentUser->id(), '=')
      ->condition('id', $nodeId, '=')
      ->execute();
    $selectedUser = $query->fetchAll();
    // If record exists.
    if ($selectedUser && $selectedUser[0]->allowed == 1) {
      // If user is allowed.
      return TRUE;
    }
    else {
      if (in_array('wiki_pages_moderator', $currentUser->getRoles()) || in_array('administrator', $currentUser->getRoles())) {
        return TRUE;
      }
      return FALSE;
    }
    return FALSE;
  }

}
