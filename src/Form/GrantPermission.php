<?php

namespace Drupal\wiki_pages\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\wiki_pages\Helper\UserWikiPages;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Extending FormBase.
 *
 * @inheritdoc
 */
class GrantPermission extends FormBase {

  /*
   * Limit the query pager
   */
  const LIMIT = 50;

  /**
   * Returning form id.
   *
   * @inheritdoc
   */
  public function getFormId() {
    return 'grant_permissions_form';
  }

  /**
   * Building the form table.
   *
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Node ID.
    $pageId = $this->getRequest()->get('node')->get('nid')->value;
    // Search term.
    $term = isset($form_state->getStorage()['term']) ? $form_state->getStorage()['term'] : NULL;
    // Getting the users form database.
    $users = $this->getUsersFromDb($term);

    // Generating the table form.
    $form = [
      'field_box' => [
        'filters' => [
          '#type' => 'fieldset',
          '#title' => $this->t('Filter'),
          '#open' => TRUE,
          'field_text' => [
            '#type' => 'textfield',
            '#title' => 'Search user(s)',
            '#description' => $this->t('<em>Enter some search term here.</em>'),
          ],
          'actions' => [
            '#type' => 'actions',
            'submit' => [
              '#type' => 'submit',
              '#button_type' => 'primary',
              '#value' => $this->t('Filter'),
              '#name' => 'submit-1',
              '#submit' => ['::getSearchResults'],
            ],
            'reset' => [
              '#type' => 'submit',
              '#button_type' => 'default',
              '#value' => $this->t('Reset'),
              '#name' => 'reset',
              '#submit' => ['::formReset'],
            ],
          ],
        ],
      ],
      'table' => [
        '#type' => 'table',
        '#header' => [
          'Granted' => [
            'id' => 'select-all-checkboxes',
          ],
          'UID' => 'UID',
          'Username' => 'Username',
          'Status' => 'Status',
        ],
        '#empty' => $this->t('There is no users yet.'),
      ],
      'actions' => [
        'submit' => [
          '#type' => 'submit',
          '#button_type' => 'primary',
          '#value' => $this->t('Save Changes'),
          '#name' => 'submit-2',
        ],
      ],
    ];
    if ($term !== NULL) {
      $form['table']['#caption'] = 'Results for term: ' . $term;
    }

    // Adding rows to table.
    $form['table'] += $this->getRowsFromUsers($users, $pageId);

    if ($term == NULL) {
      $form['pager'] = [
        '#type' => 'pager',
      ];
    }
    return $form;
  }

  /**
   * Form validation.
   *
   * @inheritdoc
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate submitted form data.
  }

  /**
   * Returning the filter term.
   *
   * @param array $form
   *   Actual form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function getSearchResults(array $form, FormStateInterface &$form_state) {

    $term = $form_state->getValue('field_text');
    $form_state->set('term', $term);
    $form_state->setRebuild(TRUE);

  }

  /**
   * Resetting the form.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function formReset(array &$form, FormStateInterface $form_state) {
    $pageId = $this->getRequest()->get('node')->get('nid')->value;
    $form_state->setRedirect('entity.node.grant_permissions', ['node' => $pageId]);
  }

  /**
   * Saving form data.
   *
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Getting the node id.
    $pageId = $this->getRequest()->get('node')->get('nid')->value;
    // Getting the form rows from form state.
    $rows = $form_state->getValue('table');

    // Checking if there are no rows selected,
    // And redirecting to the form.
    if($rows == null || $rows == "")
    {
      drupal_set_message(t('Can not save empty form.'), 'warning');
      $redirect = new RedirectResponse(Url::fromRoute('entity.node.grant_permissions', ['node' => $pageId])->toString());
      $redirect->send();
      exit();
    }
    
    // Updating-inserting the values (checked/unchecked).
    foreach ($rows as $uid => $value) {
      if ($value['granted'] == 1) {
        $this->updateOrInsert($uid, $pageId, 1);
      }
      else {
        $this->updateOrInsert($uid, $pageId, 0);
      }
    }

    drupal_set_message($this->t('Successfully saved user(s).'));

  }

  /**
   * Update/insert function.
   *
   * @param int $uid
   *   User uid.
   * @param int $pageId
   *   Node ID.
   * @param int $allowed
   *   Is user allowed to access the page (0 / 1).
   *
   * @return bool
   *   Returning if its successfully inserted/updated in database.
   */
  private function updateOrInsert($uid, $pageId, $allowed) {
    $exists_query = \Drupal::database()
      ->select('users_wikipages', 'uwp')
      ->fields('uwp', ['uid', 'id', 'allowed'])
      ->condition('uid', $uid, '=')
      ->condition('id', $pageId, '=')
      ->execute();
    $entry = $exists_query->fetchAll();

    if ($entry) {
      if ($allowed == $entry[0]->allowed) {
        return TRUE;
      }
      $update = \Drupal::database()
        ->update('users_wikipages')
        ->fields(['allowed' => $allowed])
        ->condition('uid', $uid, '=')
        ->condition('id', $pageId, '=');
      if ($update->execute()) {
        return TRUE;
      }
      return FALSE;
    }
    else {
      $insert = \Drupal::database()
        ->insert('users_wikipages')
        ->fields([
          'uid',
          'id',
          'allowed',
        ])
        ->values([$uid, $pageId, $allowed]);
      if ($insert->execute()) {
        return TRUE;
      }
      return FALSE;
    }

  }

  /**
   * Generating the form table rows.
   *
   * @param array $users
   *   Users array.
   * @param int $pageId
   *   Node ID.
   *
   * @return array
   *   Formatting the form table rows.
   */
  private function getRowsFromUsers(array $users, $pageId) {
    $returnArray = [];
    foreach ($users as $id => $user) {
      if ($user->uid == 0) {
        continue;
      }

      $user = User::load($user->uid);

      $returnArray[$user->get('uid')->value] = [
        'granted' => [
          '#type' => 'checkbox',
          '#default_value' => UserWikiPages::isPreSelected($pageId, $user->get('uid')->value),
          '#id' => $user->get('uid')->value,
        ],
        'uid' => [
          '#plain_text' => $user->get('uid')->value,
        ],
        'username' => [
          '#plain_text' => $user->getUsername(),
        ],
        'status' => [
          '#plain_text' => ($user->isActive() ? 'Active' : 'Blocked'),
        ],
      ];
    }
    return $returnArray;
  }

  /**
   * Getting users from database.
   *
   * @param string $term
   *   Searching term.
   *
   * @return array
   *   Returning an array of found users.
   */
  private function getUsersFromDb($term = NULL) {
    $query = \Drupal::database()
      ->select('users_field_data', 'ufd')
      ->fields('ufd', ['uid', 'name'])
      ->condition('uid', 0, '!=');
    if ($term !== NULL) {
      $query->condition('name', '%' . $term . '%', 'LIKE');
    }
    $query->orderBy('name');
    if ($term == NULL) {
      $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')
        ->limit(self::LIMIT);
      $users = $pager->execute()->fetchAll();
    }
    else {
      $users = $query->execute()->fetchAll();
    }
    return $users;
  }

}
