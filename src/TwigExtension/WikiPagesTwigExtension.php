<?php

namespace Drupal\wiki_pages\TwigExtension;

use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\wiki_pages\Helper\UserWikiPages;

/**
 * Extending Twig_Extension for custom functionality.
 */
class WikiPagesTwigExtension extends \Twig_Extension {

  /**
   * Returning a name of extension.
   *
   * @inheritdoc
   */
  public function getName() {
    return 'wiki_pages.twig_extension';
  }

  /**
   * Getting the functions of custom extension.
   *
   * @inheritdoc
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('getContentMenu', [$this, 'getContentMenu']),
      new \Twig_SimpleFunction('getNewPageButton', [$this, 'getNewPageButton']),
    ];
  }

  /**
   * Returning the markup for content menu.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Node from which code find the parent node.
   *
   * @return array
   *   Returning the render markup.
   */
  public function getContentMenu(Node $node) {
    $id = $this->findParent($node->id());
    $menuArray = $this->generateMenuArray([Node::load($id)]);

    $markup = '<div class="wiki-pages-content-menu"><h2>' . t('Content') . '</h2>';
    $markup .= '<a href="' . Url::fromRoute('wiki_pages.list')
      ->toString() . '" id="back-to-wiki-pages-home">Back to home</a>';

    if ($menuArray !== NULL) {
      $markup .= $this->formatMenu($menuArray);
    }
    else {
      $markup .= '<strong>No subpages found.</strong>';
    }

    $markup .= '</div>';

    return [
      '#markup' => $markup,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Generating menu array.
   *
   * @param array $nodes
   *   Array of nodes which will be used to generate the menu array.
   *
   * @return array
   *   Menu array.
   */
  private function generateMenuArray(array $nodes) {
    $subpages = [];
    foreach ($nodes as $page) {
      $id = $page->id();
      $ids = \Drupal::entityQuery('node')
        ->condition('field_wiki_page_parent', $id, '=')
        ->execute();
      $res = Node::loadMultiple($ids);
      if ($res == NULL) {
        $subpages[$id] = [
          'id' => $id,
          'page' => $page->get('title')->value,
          'subpages' => NULL,
        ];
      }
      else {
        $subs = $this->generateMenuArray($res);
        $subpages[$id] = [
          'id' => $id,
          'page' => $page->get('title')->value,
          'subpages' => $subs,
        ];
      }
    }
    return $subpages;
  }

  /**
   * Formatting the content menu list.
   *
   * @param array $array
   *   Nodes array which will be formatted in list.
   *
   * @return string
   *   Markup list of nodes for content menu.
   */
  private function formatMenu(array $array) {
    $out = "<ol>";
    foreach ($array as $key => $item) {
      if ($key == \Drupal::routeMatch()->getRawParameter('node')) {
        $out .= "<li>";
        $out .= $item['page'];
      }
      else {
        $out .= "<li><a href='$key'>";
        $out .= $item['page'];
        $out .= "</a>";
      }
      if (is_array($item['subpages'])) {
        $out .= $this->formatMenu($item['subpages']);
      }
      $out .= "</li>";
    }
    $out .= "</ol>";
    return $out;
  }

  /**
   * Find parent scope.
   *
   * @param int $id
   *   ID of current node.
   *
   * @return int
   *   Returning an parent node id.
   */
  private function findParent($id) {
    $node = Node::load($id);
    static $parentId = NULL;
    if ($node->get('field_wiki_page_parent')->target_id !== NULL) {
      $this->findParent($node->get('field_wiki_page_parent')->target_id);
    }
    else {
      $parentId = $id;
    }
    return $parentId;
  }

  /**
   * Generating the new page button.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Node which will be used as parent in add form.
   *
   * @return array
   *   Rendering array.
   */
  public function getNewPageButton(Node $node) {
    $markup = '';
    if (UserWikiPages::isAllowed($node->get('nid')->value)) {
      $markup = '<a href="/node/add/wiki_page?field_wiki_page_parent=' . $node->get('nid')->value . '" class="button">' . t('Add new sub page') . '</a>';
    }
    return [
      '#markup' => $markup,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
