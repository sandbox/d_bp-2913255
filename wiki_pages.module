<?php

/**
 * @file
 * Module file for wiki_pages.
 */

use Drupal\Core\Form\FormState;
use Drupal\node\Entity\Node;
use Drupal\wiki_pages\Helper\UserWikiPages;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function wiki_pages_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.wiki_pages':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t("You can grant <strong>access</strong> for pages that don't have parent page. By doing that you get access to all it\'s subpages and the rights to add sub-pages and <em>edit/delete/move</em> <strong>own</strong> sub-page. When you go to your newly created Wiki page, you will see the Title, Description, Content Menu and your Content which are predefined fields by content-type.<br>All sub-pages will be displayed in Content menu where you can navigate to each sub-page.<br><br>You can see your <strong>Wiki pages list <a href=':list'>here</a></strong> and you can add new  <strong>Wiki page <a href=':add'
    >here</a></strong>.", [
          ':list' => Url::fromRoute('wiki_pages.list')
            ->toString(),
          ':add' => Url::fromUserInput('/node/add/wiki_page')->toString(),
        ]) . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('<strong><em>Creating parent Wiki Page</em></strong>') . '</dt>';
      $output .= '<dd>' . t('To do this, you need to go <strong><a href=":create">here</a></strong> and fill up all fields except the <em>parent</em> one. When you submit that form, you will be redirected to your first Wiki page.', [
          ':create' => Url::fromUserInput('/node/add/wiki_page')
            ->toString(),
        ]) . '<br>' . t('You can do the same thing by visiting the <a href=":wiki"><strong>/wiki</strong></a> route, there you will find a button which will redirect you back to add form, or you could go through your admin menu <strong>Content >> Add Content >> Wiki Page</strong>.', [
          ':wiki' => Url::fromRoute('wiki_pages.list')
            ->toString(),
        ]) . '</dd>';
      $output .= '<dt>' . t('<strong><em>Adding User Role Wiki pages moderator</em></strong>') . '</dt>';
      $output .= '<dd>' . t("In order to do such a thing you need to do the following. Navigate yourself to <strong><a href=':people'>People</a></strong> and from select box choose the <em>Add the Wiki Pages Moderator role to the selected users</em>. Select the checkbox for each user you want to add to this role and click <em>Apply to selected items</em>.<br>That's all you are done!", [
          ':people' => Url::fromUserInput('/admin/people')
            ->toString(),
        ]) . '</dd>';
      $output .= '<dt>' . t('<strong><em>Administering Wiki Pages</em></strong>') . '</dt>';
      $output .= '<dd>' . t("For this action you need to perform the same thing as for the regular nodes. Navigate yourself to the desired page, click <em>Edit</em> and change the necessary fields. The users with Wiki Pages Moderator role are able to perform this action to.<br><em>***Note: Wiki Pages Moderator role by default affects only the Nodes with < em><strong > wiki_page</strong ></em > content type .</em >") . '</dd>';
      $output .= '<dt>' . t('<strong><em>Giving users access permission</em></strong>') . '</dt>';
      $output .= '<dd>' . t('In order to give users access to your Wiki page and sub-pages, you need to go to your parent Wiki page then click on the <em><strong>Grant Permissions</strong></em> tab. After that you will be redirected to form, where you will be able to filter desired users, or search the users through list. When you find the particular user, check the checkbox and click <em>Save</em>. <strong>Make sure that you press save on each list, switching to next page will not affect the previous one.</strong> Users which are checked are able to access particular page and all sub-pages of that page, if not access is restricted.') . '</dd>';
      $output .= '<dt>' . t('<strong><em>Deletion of module</em></strong>') . '</dt>';
      $output .= '<dd>' . t('To delete this module, you simply need to go to <em><strong>Extend >> Uninstall</strong></em> and select the Wiki pages module and click <em>Uninstall</em> like for any other contributed module.') . '<br><br>' . t('<em><strong>***Note:</strong> Performing this action, will result in lose of every node which is content-type is wiki_page and users will be removed completely from Wiki pages moderator role. Module clean up everything after itself, so your Drupal web site would run smoothly.</em>') . '</dd>';
      $output .= '</dl>';
      return $output;
  }
  return NULL;
}

/**
 * Implements hook_page_attachments().
 */
function wiki_pages_page_attachments(array & $attachments) {
  $attachments['#attached']['library'][] = 'wiki_pages/wiki-pages';
}

/**
 * Implements hook_form_FORM_ID_alter() for template block.
 */
function wiki_pages_form_node_wiki_page_form_alter(&$form, FormState $form_state, $form_id) {
  // Defining the default value for $id.
  $id = NULL;

  if (\Drupal::request()->getMethod() == "GET") {
    $id = \Drupal::request()->get('field_wiki_page_parent');
  }

  if ($id !== NULL) {
    $node = Node::load($id);
    if ($node) {
      if (UserWikiPages::isAllowed(UserWikiPages::findParent($id))) {
        $form['field_wiki_page_parent']['widget']['#required'] = TRUE;
        $form['field_wiki_page_parent']['widget'][0]['target_id']['#attributes'] = [
          'readonly' => 'readonly',
        ];
        $form['field_wiki_page_parent']['widget'][0]['target_id']['#default_value'] = $node;
      }
      else {
        $response = new RedirectResponse(Url::fromRoute('wiki_pages.list', [], ['absolute' => TRUE])
          ->toString());
        $response->send();
        drupal_set_message(t('You are not allowed to insert page in this section!'), 'error', TRUE);
        exit();
      }
    }
  }
}

/**
 * Implements hook_theme().
 */
function wiki_pages_theme(array $variables) {
  $theme = [];

  $theme = [
    'node__wiki_page' => [
      'render element' => 'content',
      'base hook' => 'node',
      'template' => 'node--wiki-page',
      'path' => drupal_get_path('module', 'wiki_pages') . '/templates',
    ],
  ];
  return $theme;
}

/**
 * Implements hook_node_presave().
 */
function wiki_pages_node_presave(Node $node) {
  $currentUser = User::load(\Drupal::currentUser()->id());
  // Checking the content-type.
  if ($node->get('type')->target_id == 'wiki_page') {
    // Checking if node has parent node.
    if ($node->get('field_wiki_page_parent')->target_id == NULL || !isset($node->get('field_wiki_page_parent')->target_id)) {
      // Checking if current user can insert parent wiki page.
      if (!in_array('wiki_pages_moderator', $currentUser->getRoles()) && !in_array('administrator', $currentUser->getRoles())) {
        $response = new RedirectResponse(Url::fromRoute('wiki_pages.list', [], ['absolute' => TRUE])
          ->toString());
        $response->send();
        drupal_set_message(t('You are not allowed to insert page in this section!'), 'error', TRUE);
        exit();
      }
      // Making sure after edit that node will be
      // saved properly in users_wikipages.
      if ($node->id() != NULL) {
        _update_user_wikipages_table($node, TRUE);
      }
    }
    // If node does have parent node.
    else {
      // Checking if user is allowed to access the parent node.
      if (!UserWikiPages::isAllowed(UserWikiPages::findParent($node->get('field_wiki_page_parent')->target_id))) {
        $response = new RedirectResponse(Url::fromRoute('wiki_pages.list', [], ['absolute' => TRUE])
          ->toString());
        $response->send();
        drupal_set_message(t('You are not allowed to insert page in this section!'), 'error', TRUE);
        exit();
      }
      // Making sure after making parent page to subpage.
      // that entries are deleted from users_wikipages.
      $query = \Drupal::database()
        ->delete('users_wikipages')
        ->condition('id', $node->id(), '=')
        ->execute();
    }
  }
}

/**
 * Implements hook_node_insert().
 */
function wiki_pages_node_insert(Node $node) {
  if ($node->get('type')->target_id == 'wiki_page') {
    drupal_register_shutdown_function('_update_user_wikipages_table', $node);
  }
}

/**
 * Updating users_wikipages table with default value.
 *
 * @param \Drupal\node\Entity\Node $node
 *   Current node which is being saved.
 */
function _update_user_wikipages_table(Node $node, $reset = FALSE) {
  $id = \Drupal::database()
    ->select('node', 'nd')
    ->fields('nd', ['nid'])
    ->condition('uuid', $node->get('uuid')->value, '=')
    ->execute()
    ->fetchAll();
  $parentNodeId = $id[0]->nid;
  if ($reset == TRUE) {
    $parentNodeId = $node->id();
  }

  $userIds = \Drupal::entityQuery('user')
    ->condition('uid', 0, '!=')
    ->execute();

  $users = User::loadMultiple($userIds);

  $values = [];
  foreach ($users as $user) {
    if (UserWikiPages::exists($parentNodeId, $user->id())) {
      continue;
    }
    else {
      $values[] = [
        'uid' => $user->id(),
        'id' => $parentNodeId,
        'allowed' => 0,
      ];
    }
  }

  $query = \Drupal::database()
    ->insert('users_wikipages')
    ->fields(['uid', 'id', 'allowed']);
  foreach ($values as $record) {
    $query->values($record);
  }
  $query->execute();
}
