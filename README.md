#CONTENT OF THIS FILE

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


##INTRODUCTION

The module creates **custom content-type**, called **'Wiki page'**.
This content-type requires 4 fields, **title (required field)**, **description**, **parent (entity reference to the parent node)** and **content** where you should put your content of the page.
As with other content-types, you can freely add more fields to your wiki page, but **in that case you should override the theme for displaying all fields you have**.
Along with custom content-type, **module creates new role called 'Wiki pages moderator'**.

Users with this role can assist you with **managing all wiki pages**, editing the title, content, description and parent. **They also can manage users which can access those nodes**.
**Authenticated users, if access is granted** to one parent Wiki page, **can add/edit/remove own wiki pages** inside that **parent node.**

They are not allowed to add sub-pages to rest of the parent pages. With that in mind, users can not see the other parent pages and sub-pages which exists but they have no access to them.
**When parent page is created, you have to grant access for each user** which you want to be able to see, access, add/remove/edit (own pages).
You can do this easily through **Grant Permission tab**, which appears after you create the parent node.


By accessing the Grant permission tab, you will see table which shows you all the users in the database. You can use filter to find particular user or go through pages. **By checking and saving the form, you will grant the permission to selected users. By un checking them the permission will be revoked and the access will be restricted.**

###Basic workflow is like shown bellow:

  1. Create your parent Wiki page. (**Content >> Add Content >> Wiki Page** OR go to **/wiki** and press the **button** which will redirect you to same location)
  1. Give users access to that page from Grant Permissions. (**Check the checkboxes for users you want**)
  1. You are done!

Also make sure to check this out:

 * For a full description of th module, visit the project page:
  [Wiki Pages](http://google.com)
 * To submit bug reports and feature suggestions, or to track changes:
  [Wiki Pages feedback](http://google.com)
  

##REQUIREMENTS

This module does not require any other modules.


##INSTALLATION

 * Install as you would normally install a contributed Drupal Module. Visit: [link to visit](http://google.com) for further information.
 * You may want to rebuild the cache.
 
 
##CONFIGURATION

The module has no menu or modifiable settings. There is no configuration.

When enabled, the module will give you the possibility to create wiki pages content-type which you can add like every other content-type in **Content >> Add Content** or you can go to **/wiki** (where you can find all wiki pages) and press the button which will automatically redirect you to add page.


##TROUBLESHOOTING
 
 - If the content menu does not display, do the following:
     - Rebuild site cache in **Configuration >> Development >> Performance >> Clear all caches**.


##FAQ

**Q:** I enabled the module and authenticated user can not add Wiki page?

**A:** Check the permissions for authenticated user in **People >> Permissions** and add the following permissions:

  * ***Wiki Page:* Create new content**
  * ***Wiki Page:* Delete own content**
  * ***Wiki Page:* Edit own content**

**Q:** I created parent Wiki page with sub-pages and users can not access them?

**A:** When you create the parent Wiki page, you need to add access permissions to each user you want to access the page.
You need to go to your parent Wiki page, then click on the Grant permissions tab, from there you can easily select users you want to access.

******Note: If you grant access to particular parent Wiki page, the user have access to whole tree, and he can add/remove own wiki pages in that parent.***


##MAINTAINERS

Current maintainers:

  * [Nebojsa Pejnovic](https://www.drupal.org/u/pejka)
  * [Zoran Tovarloza](https://www.drupal.org/u/z.tovarloza)
  * [Boris Pupavac](https://www.drupal.org/u/d_bp)
  
This project has been sponsored by:

  * **Bee IT LTD**
  Specialized in consulting and planning of Drupal powered sites, Bee IT offers installation, development, theming and customization to get you started.
  
Visit: [www.beeit.rs](http://beeit.rs)