/**
 * @file
 */

jQuery(document).ready(function () {
  // Adding check box in table header.
  jQuery('#select-all-checkboxes').append('<input type="checkbox" id="select-all-wiki_form">');

  // Checking if all checkboxes are preselected.
  if (jQuery("input:checkbox:checked").length == jQuery('input:checkbox').length - 1) {
    jQuery('#select-all-wiki_form').prop('checked', 'checked');
  }
  else {
    jQuery('#select-all-wiki_form').prop('checked', false);
  }
  // Attaching onclick event to generated checkbox.
  jQuery("#select-all-wiki_form").click(function () {
    jQuery('input:checkbox').not(this).prop('checked', this.checked);
  });

  // Attaching onclick event to all form checkboxes, and checking if they are
  // all checked if so checking the header check box if not unchecking the
  // check box.
  jQuery(".form-checkbox").click(function () {
    var arr = [];

    jQuery(".form-checkbox").each(function () {
      arr.push(jQuery(this).prop('checked'));
    });

    if (jQuery('input:checkbox').length - 1 == checkedBoxes(arr)) {
      jQuery('#select-all-wiki_form').prop('checked', 'checked');
    }
    else {
      jQuery('#select-all-wiki_form').prop('checked', false);
    }
  });

  // Returning an number of checked checkboxes.
  function checkedBoxes(array) {
    var countTrue = 0;
    for (i = 0; i < array.length; i++) {
      if (array[i] == true) {
        countTrue++;
      }
    }
    return countTrue;
  }
});
